package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name string `json:"name" form:"name" binding:"required"`
	Email string `json:"email" form:"email" binding:"required"`
}

func CreateUser(db *gorm.DB, User *User) (err error)  {
	err = db.Create(&User).Error
	if err != nil {
		return err
	}
	return nil
}

func SelectUser(db *gorm.DB, User *[]User) (err error) {
	err = db.Find(&User).Error
	if err != nil {
		return err
	}
	return nil
}

func SelectUserById(db *gorm.DB, user *User, id string) (err error) {
	err = db.Where("id=?", id).First(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateUser(db *gorm.DB, User *User) (err error) {
	db.Save(&User)
	return nil
}

func DeleteUser(db *gorm.DB, User *User, id string) (err error) {
	db.Where("id=?", id).Delete(&User)
	return nil
}